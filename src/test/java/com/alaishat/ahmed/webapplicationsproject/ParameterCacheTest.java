package com.alaishat.ahmed.webapplicationsproject;

import com.alaishat.ahmed.webapplicationsproject.exceptions.AppException;
import com.alaishat.ahmed.webapplicationsproject.exceptions.ResourceNotFoundException;
import com.alaishat.ahmed.webapplicationsproject.models.Parameter;
import com.alaishat.ahmed.webapplicationsproject.services.ParameterService;
import org.junit.Assert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cache.CacheManager;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Optional;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = App.class)
public class ParameterCacheTest {

    @Autowired
    CacheManager cacheManager;

    @Autowired
    ParameterService service;

    @BeforeEach
    void setUp() throws Exception {
        service.save(new Parameter("A", "AA"));
        service.save(new Parameter("test", "testtest"));
    }

    private Optional<Parameter> getCachedParameter(String key) {
        return Optional.ofNullable(cacheManager.getCache("parameters")).map(c -> c.get(key, Parameter.class));
    }

    @Test
    void givenParameterThatShouldBeCached_whenFindById_thenResultShouldBePutInCache() throws ResourceNotFoundException {
        Parameter dune = service.findByKey("A");
        Assert.assertTrue(getCachedParameter("A").isPresent());
        Assert.assertEquals(dune, getCachedParameter("A").get());
    }

    @Test
    void givenParameterThatShouldBePutInCacheWhenUpdate() throws AppException {
        service.save(new Parameter("A", "A"));
        Parameter updated = service.updateByKey(new Parameter("A", "AA"));
        Assert.assertTrue(getCachedParameter("A").isPresent());
        Assert.assertEquals(updated, getCachedParameter("A").get());
    }

    @Test
    void givenParameterThatShouldNotBeCached_whenFindById_thenResultShouldNotBePutInCache() throws ResourceNotFoundException {
        service.findByKey("test");
        Assert.assertEquals(Optional.empty(), getCachedParameter("test"));
    }

    @Test
    void givenParameterThatShouldBeEvictedFromCache_whenDeleteById() throws ResourceNotFoundException {
        service.deleteByKey("A");
        Assert.assertEquals(Optional.empty(), getCachedParameter("A"));
    }
}