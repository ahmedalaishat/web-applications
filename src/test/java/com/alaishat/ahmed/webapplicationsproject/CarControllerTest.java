package com.alaishat.ahmed.webapplicationsproject;

import com.alaishat.ahmed.webapplicationsproject.models.AppUser;
import com.alaishat.ahmed.webapplicationsproject.models.Car;
import com.alaishat.ahmed.webapplicationsproject.services.CarService;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import lombok.SneakyThrows;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.supercsv.io.CsvBeanWriter;
import org.supercsv.io.ICsvBeanWriter;
import org.supercsv.prefs.CsvPreference;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;


public class CarControllerTest extends AbstractTest {

    private String token = "";

    @Autowired
    CarService carService;

    @SneakyThrows
    @Override
    @Before
    public void setUp() {
        super.setUp();
//        setup users
        AppUser appUser = new AppUser("B", "B");
        String userJson = new Gson().toJson(appUser);
        var registerResult = mvc.perform(MockMvcRequestBuilders.post("/register")
//                  .header("Authorization","eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJiIiwiZXhwIjoxNjA5OTg4MzY1fQ.l3V0UXrcM8bSVhJGJW7QjvuDchG1yCMHByGrEbA1Y4W2aIU_sR9FFOBASYQvJmdJyXrjnqyeMXHHszvxUEbQRA")
                .contentType(MediaType.APPLICATION_JSON_VALUE).content(userJson)).andReturn();
        var loginResult = mvc.perform(MockMvcRequestBuilders.post("/login")
//                  .header("Authorization","eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJiIiwiZXhwIjoxNjA5OTg4MzY1fQ.l3V0UXrcM8bSVhJGJW7QjvuDchG1yCMHByGrEbA1Y4W2aIU_sR9FFOBASYQvJmdJyXrjnqyeMXHHszvxUEbQRA")
                .contentType(MediaType.APPLICATION_JSON_VALUE).content(userJson)).andReturn();
        JsonObject jsonObject = (JsonObject) JsonParser.parseString(loginResult.getResponse().getContentAsString());
        token = jsonObject.getAsJsonObject("body").getAsJsonPrimitive("token").getAsString();
        // read JSON and load json
        ObjectMapper mapper = new ObjectMapper();
        TypeReference<List<Car>> typeReference = new TypeReference<List<Car>>() {
        };
        InputStream inputStream = TypeReference.class.getResourceAsStream("/json/MOCK_DATA.json");
        try {
            List<Car> cars = mapper.readValue(inputStream, typeReference);
            carService.saveAll(cars);
            System.out.println("Cars Saved!");
        } catch (IOException e) {
            System.out.println("Unable to save cars: " + e.getMessage());
        }
    }

    /*  @Test
      public void getCarsList() throws Exception {
      String uri = "/products";
      MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri).accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
      int status = mvcResult.getResponse().getStatus();
      assertEquals(200, status);
      String content = mvcResult.getResponse().getContentAsString();
      Car[] productlist = super.mapFromJson(content, Car[].class);
      assertTrue(productlist.length > 0);
} */
    @Test
    public void createCar() throws Exception {
        String uri = "/cars";
        Car car = new Car();
        car.setCarId((long) 1);
        car.setName("bb");
        car.setNumOfSeats(4);
        car.setPrice((long) 300);
        // product.setSaleDate(2020,12\01);
        car.setSalePrice((long) 200);
        car.setBuyerName("raneem");
        //car.setSaleDate(date);
        car.setCarId((long) 2);
        String inputJson = new Gson().toJson(car);
        var mvcResult1 = mvc.perform(MockMvcRequestBuilders.post(uri)
                .header("Authorization", token)
                .contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson)).andReturn();
        int status = mvcResult1.getResponse().getStatus();
        assertEquals(201, status);
        String content = mvcResult1.getResponse().getContentAsString();
        assertEquals(content, "Product is created successfully");
    }

    @Test
    public void updateCar() throws Exception {
        String uri = "/cars/4";
        Car car = new Car();
        car.setName("car");
        car.setCarId((long) 4);
        car.setNumOfSeats(4);
        car.setPrice((long) 500);
        String inputJson = super.mapToJson(car);
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.put(uri).contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson)).andReturn();
        int status = mvcResult.getResponse().getStatus();
        assertEquals(200, status);
        String content = mvcResult.getResponse().getContentAsString();
        assertEquals(content, "Car is updated successsfully");
    }

    ///...........
    public void testUpdateCarNotfoundCase() throws Exception {
        String uri = "/cars/8";
        Car car2 = new Car();
        car2.setName("car");
        car2.setCarId((long) 4);
        car2.setNumOfSeats(4);
        car2.setPrice((long) 500);
        String inputJson = super.mapToJson(car2);
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.put(uri).contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson)).andReturn();
        int status = mvcResult.getResponse().getStatus();
        assertEquals(200, status);
        String content = mvcResult.getResponse().getContentAsString();
        assertEquals(content, "Car is Not Found");
    }

    @Test
    public void deleteById() throws Exception {
        String uri = "/cars/{2}}";
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.delete(uri)).andReturn();
        int status = mvcResult.getResponse().getStatus();
        assertEquals(200, status);
        String content = mvcResult.getResponse().getContentAsString();
        assertEquals(content, "Car is Not Found ");
    }

    //....................
    @Test
    public void testDeleteByIdNotFound() throws Exception {
        String uri = "/cars/{10}}";
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.delete(uri)).andReturn();
        int status = mvcResult.getResponse().getStatus();
        assertEquals(200, status);
        String content = mvcResult.getResponse().getContentAsString();
        assertEquals(content, "Car is Not Found ");
    }

    //....export csv file
    public void exportToCSV(HttpServletResponse response) throws IOException {
        response.setContentType("text/csv");
        DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss");
        String currentDateTime = dateFormatter.format(new Date());
        String headerKey = "Content-Disposition";
        String headerValue = "attachment; filename=cars_" + currentDateTime;
        List<Car> listCars = carService.findAll("all");
        //  File csvfile = new File("text/csv");
        ICsvBeanWriter csvWriter = new CsvBeanWriter(response.getWriter(), CsvPreference.STANDARD_PREFERENCE);
        String[] csvHeader = {"carId", "price", "Buyer Name"};
        String[] nameMapping = {"id", "email", "fullName", "roles"};
        csvWriter.writeHeader(csvHeader);
        for (Car car : listCars) {
            csvWriter.write(car, nameMapping);
        }
        csvWriter.close();
    }
}


