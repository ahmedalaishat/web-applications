package com.alaishat.ahmed.webapplicationsproject;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import javax.sql.DataSource;

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;

@SpringBootTest
public class ConnectionPoolingApplicationTests {
    @Autowired
    private DataSource dataSource;

    @Test
   public void contextLoads() {
        assertThat(dataSource).isNotNull();     }

}
