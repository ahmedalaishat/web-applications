package com.alaishat.ahmed.webapplicationsproject.services;

import com.alaishat.ahmed.webapplicationsproject.exceptions.AppException;
import com.alaishat.ahmed.webapplicationsproject.exceptions.BadResourceException;
import com.alaishat.ahmed.webapplicationsproject.exceptions.ResourceAlreadyExistsException;
import com.alaishat.ahmed.webapplicationsproject.models.AppUser;
import com.alaishat.ahmed.webapplicationsproject.models.repositories.UserRepository;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.thymeleaf.util.StringUtils;

import java.util.List;
import java.util.Map;

import static java.util.Collections.emptyList;

@Service
public class UserService implements UserDetailsService {
    private final Logger logger = LoggerFactory.getLogger("users table affected");
    @Autowired
    private UserRepository repository;




    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        AppUser user = repository.findByUsername(username);
        if (user == null) {
            throw new UsernameNotFoundException(username);
        }
        return new User(user.getUsername(), user.getPassword(), emptyList());
    }

    public AppUser save(AppUser user) throws AppException {
        if (StringUtils.isEmpty(user.getUsername()) || StringUtils.isEmpty(user.getPassword())) {
            BadResourceException exc = new BadResourceException("Failed to save User");
            exc.addErrorMessage("Invalid Username or password");
            throw exc;
        } else {
            if (user.getUsername() != null && repository.findByUsername(user.getUsername()) != null) {
                throw new ResourceAlreadyExistsException("User with username: " + user.getUsername() +
                        " already exists");
            }
            AppUser saved = repository.save(user);
            logger.info("User with id " + user.getUserId() + " saved successfully.");
            return saved;
        }
    }

    public static String getUsername() {
        try {
            return ((Map<String, String>)
                    SecurityContextHolder.getContext().getAuthentication().getPrincipal())
                    .get("sub");
        }catch (Exception e){
            return ((UserDetails)
                    SecurityContextHolder.getContext().getAuthentication().getPrincipal())
                    .getUsername();
        }
    }
//    private boolean existsById(Long id) {
//        return repository.existsById(id);
//    }
//
//    public AppUser findById(Long id) throws UserNotFoundException {
//        AppUser user = repository.findById(id).orElse(null);
//        if (user == null)
//            throw new UserNotFoundException("Cannot find User with id: " + id);
//        else return user;
//    }
//
//    public List<AppUser> findAll(int pageNumber, int rowPerPage) {
//        List<AppUser> users = new ArrayList<>();
//        Pageable sortedByIdAsc = PageRequest.of(pageNumber - 1, rowPerPage,
//                Sort.by("id").ascending());
//        repository.findAll(sortedByIdAsc).forEach(users::add);
//        return users;
//    }
//
//    public void update(AppUser user)
//            throws BadResourceException, ResourceNotFoundException {
//        if (!StringUtils.isEmpty(user.getUsername())) {
//            if (!existsById(user.getUserId())) {
//                throw new ResourceNotFoundException("Cannot find User with id: " + user.getUserId());
//            }
//            repository.save(user);
//        } else {
//            BadResourceException exc = new BadResourceException("Failed to save User");
//            exc.addErrorMessage("Invalid username or password");
//            throw exc;
//        }
//    }
//
//    public void deleteById(Long id) throws ResourceNotFoundException {
//        if (!existsById(id)) {
//            throw new ResourceNotFoundException("Cannot find user with id: " + id);
//        } else {
//            repository.deleteById(id);
//        }
//    }
//
//    public Long count() {
//        return repository.count();
//    }

}