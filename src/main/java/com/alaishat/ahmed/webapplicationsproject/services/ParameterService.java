package com.alaishat.ahmed.webapplicationsproject.services;

import com.alaishat.ahmed.webapplicationsproject.exceptions.AppException;
import com.alaishat.ahmed.webapplicationsproject.exceptions.BadResourceException;
import com.alaishat.ahmed.webapplicationsproject.exceptions.ResourceNotFoundException;
import com.alaishat.ahmed.webapplicationsproject.models.Parameter;
import com.alaishat.ahmed.webapplicationsproject.models.repositories.ParameterRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.thymeleaf.util.StringUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by IntelliJ IDEA
 * Author: Ahmed Al-Aishat
 * Date: 11/25/2020
 * Time: 3:37 PM
 */
@Service
public class ParameterService {
    private final Logger logger = LoggerFactory.getLogger("parameters table affected");

    @Autowired
    private ParameterRepository repository;

    public List<Parameter> findAll() {
        List<Parameter> parameters = new ArrayList<>();
        repository.findAll().forEach(parameters::add);
        return parameters;
    }

    public Parameter save(Parameter parameter) throws AppException {
        if (StringUtils.isEmpty(parameter.getKey())) {
            BadResourceException exc = new BadResourceException("Failed to save parameter");
            exc.addErrorMessage("parameter's key is empty");
            throw exc;
        }
        if (StringUtils.isEmpty(parameter.getValue())) {
            BadResourceException exc = new BadResourceException("Failed to save parameter");
            exc.addErrorMessage("parameter's value is empty");
            throw exc;
        }
        parameter.setCreatedBy(UserService.getUsername());
        parameter.setUpdatedBy(UserService.getUsername());
        Parameter saved = repository.save(parameter);
        logger.info("Parameter with key " + parameter.getKey() + " saved successfully.");
        return saved;
    }

    private boolean existsByKey(String key) {
        return repository.existsById(key);
    }

    public void deleteByKey(String key) throws ResourceNotFoundException {
        if (!existsByKey(key)) {
            throw new ResourceNotFoundException("Cannot find parameter with key: " + key);
        } else {
            repository.deleteById(key);
            logger.info("Parameter with key " + key + " deleted successfully.");
        }
    }

    public Parameter findByKey(String key) throws ResourceNotFoundException {
        return repository.findParameterByKey(key).orElseThrow(
                () -> new ResourceNotFoundException("Cannot find parameter with key: " + key)
        );
    }

    public Parameter updateByKey(Parameter newParameterInfo) throws ResourceNotFoundException, BadResourceException {
        if (StringUtils.isEmpty(newParameterInfo.getKey())) {
            BadResourceException exc = new BadResourceException("Failed to update parameter");
            exc.addErrorMessage("parameter's name is empty");
            throw exc;
        }
        if (StringUtils.isEmpty(newParameterInfo.getValue())) {
            BadResourceException exc = new BadResourceException("Failed to update parameter");
            exc.addErrorMessage("parameter's value is empty");
            throw exc;
        }
        return repository.findParameterByKey(newParameterInfo.getKey()).map(parameter -> {
            parameter.setValue(newParameterInfo.getValue());
            parameter.setUpdatedBy(UserService.getUsername());
            Parameter updated = repository.save(parameter);
            logger.info("Parameter with key " + parameter.getKey() + " updated successfully.");
            return updated;
        }).orElseThrow(() -> new ResourceNotFoundException("Cannot find parameter with key: " + newParameterInfo.getKey()));
    }
}
