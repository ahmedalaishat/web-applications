package com.alaishat.ahmed.webapplicationsproject.services;

import com.alaishat.ahmed.webapplicationsproject.exceptions.AppException;
import com.alaishat.ahmed.webapplicationsproject.exceptions.BadResourceException;
import com.alaishat.ahmed.webapplicationsproject.exceptions.ResourceAlreadyExistsException;
import com.alaishat.ahmed.webapplicationsproject.exceptions.ResourceNotFoundException;
import com.alaishat.ahmed.webapplicationsproject.models.Car;
import com.alaishat.ahmed.webapplicationsproject.models.Parameter;
import com.alaishat.ahmed.webapplicationsproject.models.repositories.CarRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.thymeleaf.util.StringUtils;

import javax.persistence.LockModeType;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Created by IntelliJ IDEA
 * Author: Ahmed Al-Aishat
 * Date: 11/21/2020
 * Time: 3:17 PM
 */
@Service
public class CarService {
    private final Logger logger = LoggerFactory.getLogger("cars table affected");

    @Autowired
    private CarRepository repository;

    @Autowired
    private ParameterService parameterService;

    public Car findById(Long id, LockModeType pessimisticRead) throws ResourceNotFoundException {
        return repository.findById(id).orElseThrow(
                () -> new ResourceNotFoundException("Cannot find car with id: " + id)
        );
    }

    public List<Car> findAll(int pageNumber, int rowPerPage) {
        List<Car> cars = new ArrayList<>();
        Pageable sortedByIdAsc = PageRequest.of(pageNumber - 1, rowPerPage,
                Sort.by("id").ascending());
        repository.findAll(sortedByIdAsc).forEach(cars::add);
        return cars;
    }

    public List<Car> findAll(String status) {
        List<Car> cars = new ArrayList<>();
        if (status.equals(Car.SOLD))
            repository.findBySaleDateNotNull().forEach(cars::add);
        else if (status.equals(Car.UNSOLD))
            repository.findBySaleDateNull().forEach(cars::add);
        else if (status.equals(Car.ALL))
            repository.findAll().forEach(cars::add);
        return cars;
    }

    //    @OptimisticlyLocked
    public Car save(Car car) throws AppException {
        if (StringUtils.isEmpty(car.getName())) {
            BadResourceException exc = new BadResourceException("Failed to save car");
            exc.addErrorMessage("Car's name is empty");
            throw exc;
        }
        if (car.getPrice() == null) {
            BadResourceException exc = new BadResourceException("Failed to save car");
            exc.addErrorMessage("Car's price is empty");
            throw exc;
        }
        if (car.getNumOfSeats() == null) {
            Parameter parameter = parameterService.findByKey("num_of_seats");
            car.setNumOfSeats(Integer.parseInt(parameter.getValue()));
        }
        if (car.getSalePrice() == null) {
            Parameter parameter = parameterService.findByKey("profit_percentage");
            double profitPercentage = (Double.parseDouble(parameter.getValue())) / 100;
            car.setSalePrice(car.getPrice() + (Math.round(profitPercentage * car.getPrice())));
        }
        car.setCreatedBy(UserService.getUsername());
        car.setUpdatedBy(UserService.getUsername());
        Car saved = repository.save(car);
        logger.info("Car with id " + saved.getCarId() + " saved successfully.");
        return saved;
    }

    @Transactional
    public void updateBasicsFields(Car newCarInfo, Long id) throws ResourceNotFoundException, BadResourceException {
        if (StringUtils.isEmpty(newCarInfo.getName())
                && newCarInfo.getPrice() == null
                && newCarInfo.getNumOfSeats() == null) {
            BadResourceException exc = new BadResourceException("Failed to save car");
            exc.addErrorMessage("Car's name, price and number of seats are empty");
            throw exc;
        }
        repository.findByCarId(id).map(car -> {
            car.setName(newCarInfo.getName());
            car.setPrice(newCarInfo.getPrice());
            car.setNumOfSeats(newCarInfo.getNumOfSeats());
            car.setUpdatedBy(UserService.getUsername());
            Car updated = repository.save(car);
            logger.info("Car with id " + updated.getCarId() + " updated successfully.");
            return updated;
        }).orElseThrow(() -> new ResourceNotFoundException("Cannot find Car with id: " + id));
    }

    @Transactional
    public void buy(Car carToBuy, Long id) throws ResourceNotFoundException, BadResourceException, ResourceAlreadyExistsException {
        if (carToBuy.getSaleDate() == null) {
            BadResourceException exc = new BadResourceException("Failed to buy car");
            exc.addErrorMessage("Car saleDate is empty");
            throw exc;
        }
//        if (carToBuy.getSalePrice() == null) {
//            BadResourceException exc = new BadResourceException("Failed to buy car");
//            exc.addErrorMessage("Car salePrice is empty");
//            throw exc;
//        }
        if (carToBuy.getBuyerName() == null) {
            BadResourceException exc = new BadResourceException("Failed to buy car");
            exc.addErrorMessage("Car buyerName is empty");
            throw exc;
        }
        Optional<Car> optional = repository.findById(id);
        if (optional.isEmpty()) {
            throw new ResourceNotFoundException("Cannot find car with id: " + id);
        }
        Car car = optional.get();
        if (car.getSaleDate() != null) {
            throw new ResourceAlreadyExistsException("Car Already bought.");
        }
//        car.setSalePrice(carToBuy.getSalePrice());
        car.setSaleDate(carToBuy.getSaleDate());
        car.setBuyerName(carToBuy.getBuyerName());
        car.setUpdatedBy(UserService.getUsername());
        Car bought = repository.save(car);
        logger.info("Car with id " + bought.getCarId() + " bought successfully.");
    }

    public void deleteById(Long id) throws ResourceNotFoundException {
        if (!existsById(id)) {
            throw new ResourceNotFoundException("Cannot find car with id: " + id);
        } else {
            repository.deleteById(id);
            logger.info("Car with id " + id + " deleted successfully.");
        }
    }

    private boolean existsById(Long id) {
        return repository.existsById(id);
    }

    public Long count() {
        return repository.count();
    }

    public void save(List<Car> cars) {
        for (int obj = 1; obj < cars.size(); obj++) {
            repository.save(cars.get(obj));
        }

    }

    public Iterable<Car> saveAll(List<Car> cars) {
        return repository.saveAll(cars);
    }
}
