package com.alaishat.ahmed.webapplicationsproject.util;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.util.Map;

/**
 * Created by IntelliJ IDEA
 * Author: Ahmed Al-Aishat
 * Date: 11/21/2020
 * Time: 12:54 PM
 */
public class ResponseUtil {
    private static final String OK = "OK";
    private static final String ERROR = "ERROR";

    public static Map getResponseBody(String message, JsonElement body) {
        return getBody(message, body, false);
    }

    private static Map getBody(String message, JsonElement body, boolean isError) {
        JsonObject object = new JsonObject();
        object.addProperty("status", isError ? ERROR : OK);
        object.addProperty("message", message);
        object.add(isError ? "error" : "body", body);
        return new Gson().fromJson(object.toString(), Map.class);
    }

    public static Map getErrorBody(String message, JsonElement body) {
        return getBody(message, body, true);
    }
}
