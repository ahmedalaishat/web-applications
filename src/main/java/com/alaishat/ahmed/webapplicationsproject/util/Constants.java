package com.alaishat.ahmed.webapplicationsproject.util;

public class Constants {
    public static final String REGISTRATION_ENDPOINT = "/register";
    public static final String LOGIN_ENDPOINT = "/login";
    public static final String KEY = "q3t6w9z$C&F)J@NcQfTjWnZr4u7x!A%D*G-KaPdSgUkXp2s5v8y/B?E(H+MbQeTh";
    public static final String AUTH_HEADER_NAME = "Authorization";
    public static final Long EXPIRATION_TIME = 1000L * 60 * 60 * 24 * 30;
}