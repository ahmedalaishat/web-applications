package com.alaishat.ahmed.webapplicationsproject.controllers;

import com.alaishat.ahmed.webapplicationsproject.exceptions.AppException;
import com.alaishat.ahmed.webapplicationsproject.models.AppUser;
import com.alaishat.ahmed.webapplicationsproject.services.UserService;
import com.alaishat.ahmed.webapplicationsproject.util.Constants;
import com.alaishat.ahmed.webapplicationsproject.util.ResponseUtil;
import com.google.gson.JsonObject;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.security.Keys;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.net.URI;
import java.net.URISyntaxException;
import java.security.Key;
import java.util.ArrayList;
import java.util.Date;
import java.util.Map;

/**
 * Created by IntelliJ IDEA
 * Author: Ahmed Al-Aishat
 * Date: 11/18/2020
 * Time: 5:28 PM
 */
@RestController
class UsersController {
    @Autowired
    private UserService userService;
    @Autowired
    private BCryptPasswordEncoder passwordEncoder;
    @Autowired
    private AuthenticationManager authenticationManager;

    @PostMapping("/register")
    ResponseEntity store(@RequestBody AppUser newUser) throws URISyntaxException, AppException {
        newUser.setPassword(passwordEncoder.encode(newUser.getPassword()));
        //AHMED_TODO: remove password from response
        AppUser user = userService.save(newUser);
        return ResponseEntity.created(new URI("/users/" + user.getUserId())).body(user);
    }

    @PostMapping("/login")
    Map login(@RequestBody AppUser user) throws URISyntaxException, AppException {
        Authentication auth = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(user.getUsername(),
                        user.getPassword(), new ArrayList<>()));
        Date exp = new Date(System.currentTimeMillis() + Constants.EXPIRATION_TIME);
        Key key = Keys.hmacShaKeyFor(Constants.KEY.getBytes());
        Claims claims = Jwts.claims().setSubject(((User) auth.getPrincipal()).getUsername());
        String token = Jwts.builder().setClaims(claims).signWith(SignatureAlgorithm.HS512, key).setExpiration(exp).compact();
        JsonObject object = new JsonObject();
        object.addProperty("username", ((User) auth.getPrincipal()).getUsername());
        object.addProperty("token", token);
        SecurityContextHolder.getContext().setAuthentication(auth);
        return ResponseUtil.getResponseBody("Login successful!", object);
    }
}
//    @GetMapping("/users")
//    List<AppUser> index() {
//        return repository.findAll();
//    }
//    @GetMapping("/users/{id}")
//    AppUser show(@PathVariable Long id) {
//        return repository.findById(id)
//                .orElseThrow(() -> new UserNotFoundException(id));
//    }
//    @PutMapping("/users/{id}")
//    AppUser update(@RequestBody AppUser newUser, @PathVariable Long id) {
//        return repository.findById(id)
//                .map(user -> {
//                    user.setUsername(newUser.getUsername());
//                    user.setPassword(newUser.getPassword());
//                    return repository.save(user);
//                })
//                .orElseGet(() -> {
//                    newUser.setUserId(id);
//                    return repository.save(newUser);
//                });
//    }
//    @DeleteMapping("/users/{id}")
//    void delete(@PathVariable Long id) {
//        repository.deleteById(id);
//    }