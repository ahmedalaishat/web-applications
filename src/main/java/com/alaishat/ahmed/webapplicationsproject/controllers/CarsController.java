package com.alaishat.ahmed.webapplicationsproject.controllers;

import com.alaishat.ahmed.webapplicationsproject.exceptions.AppException;
import com.alaishat.ahmed.webapplicationsproject.exceptions.ResourceNotFoundException;
import com.alaishat.ahmed.webapplicationsproject.models.Car;
import com.alaishat.ahmed.webapplicationsproject.services.CarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.persistence.LockModeType;
import java.util.List;

/**
 * Created by IntelliJ IDEA
 * Author: Ahmed Al-Aishat
 * Date: 11/21/2020
 * Time: 3:22 PM
 */
@RestController
public class CarsController {
    @Autowired
    private CarService service;

    @GetMapping("/cars")
    List<Car> index(@RequestParam(required = false, defaultValue = Car.ALL) String status) {
        return service.findAll(status);
    }

    @GetMapping("/cars/{id}")
    Car show(@PathVariable Long id) throws AppException {
        return service.findById(id, LockModeType.PESSIMISTIC_READ);
    }

    @PostMapping("/cars")
    Car save(@RequestBody Car car) throws AppException {
        return service.save(car);
    }

    @PutMapping("/cars/{id}")
    void update(@RequestBody Car car, @PathVariable Long id) throws AppException {
        service.updateBasicsFields(car, id);
    }

    @PutMapping("/cars/{id}/buy")
    void buy(@RequestBody Car car, @PathVariable Long id) throws AppException {
        service.buy(car, id);
    }

    @DeleteMapping("/cars/{id}")
    void delete(@PathVariable Long id) throws ResourceNotFoundException {
        service.deleteById(id);
    }
}

