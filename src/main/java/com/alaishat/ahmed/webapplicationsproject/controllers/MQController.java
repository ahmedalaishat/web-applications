package com.alaishat.ahmed.webapplicationsproject.controllers;

import com.alaishat.ahmed.webapplicationsproject.mq.MessageService;
import com.alaishat.ahmed.webapplicationsproject.mq.ReportMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Map;

/**
 * Created by IntelliJ IDEA
 * Author: Ahmed Al-Aishat
 * Date: 11/22/2020
 * Time: 11:12 PM
 */
@RestController
public class MQController {

    @Autowired
    private MessageService messageService;

    @PostMapping("/request-cars-report")
    Map send(@RequestBody ReportMessage reportMessage) throws ParseException {
        if (reportMessage.getEmailAddress() == null)
            reportMessage = new ReportMessage("ahmedalaishat@gmail.com", "this is the report of cars Sold in november/2020",
                    new SimpleDateFormat("yyyy/MM").parse("2020/11"));
        return messageService.sendMessage(reportMessage);
    }
}
