package com.alaishat.ahmed.webapplicationsproject.controllers;

import com.alaishat.ahmed.webapplicationsproject.exceptions.AppException;
import com.alaishat.ahmed.webapplicationsproject.models.Parameter;
import com.alaishat.ahmed.webapplicationsproject.services.ParameterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by IntelliJ IDEA
 * Author: Ahmed Al-Aishat
 * Date: 11/25/2020
 * Time: 3:36 PM
 */
@RestController
public class ParametersController {
    @Autowired
    private ParameterService service;

    @GetMapping("/parameters")
    List<Parameter> index() {
        return service.findAll();
    }

    @PostMapping("/parameters")
    Parameter save(@RequestBody Parameter parameter) throws AppException {
        return service.save(parameter);
    }

    @DeleteMapping("/parameters/{key}")
    void delete(@PathVariable String key) throws AppException {
        service.deleteByKey(key);
    }

    @PutMapping("/parameters/{key}")
    void update(@RequestBody Parameter parameter) throws AppException {
        service.updateByKey(parameter);
    }

    @GetMapping("/parameters/{key}")
    Parameter show(@PathVariable String key) throws AppException {
        return service.findByKey(key);
    }
}