package com.alaishat.ahmed.webapplicationsproject;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Created by IntelliJ IDEA
 * Author: Ahmed Al-Aishat
 * Date: 11/18/2020
 * Time: 5:27 PM
 */
@SpringBootApplication
public class App {
    public static void main(String[] args) {
        SpringApplication.run(com.alaishat.ahmed.webapplicationsproject.App.class, args);
    }
}