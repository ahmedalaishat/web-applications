package com.alaishat.ahmed.webapplicationsproject.models.repositories;

import com.alaishat.ahmed.webapplicationsproject.models.Parameter;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.Optional;

/**
 * Created by IntelliJ IDEA
 * Author: Ahmed Al-Aishat
 * Date: 11/25/2020
 * Time: 3:38 PM
 */
public interface ParameterRepository extends PagingAndSortingRepository<Parameter, String>,
        JpaSpecificationExecutor<Parameter> {

    @Cacheable(value = "parameters", unless = "#a0=='test'")
    Optional<Parameter> findParameterByKey(String key);

    @CacheEvict(value = "parameters", key = "#key")
    void deleteById(String key);

    @CachePut(value = "parameters", key = "#parameter.key")
    Parameter save(Parameter parameter);
}
