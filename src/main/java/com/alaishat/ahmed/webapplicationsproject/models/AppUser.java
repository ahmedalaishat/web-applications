package com.alaishat.ahmed.webapplicationsproject.models;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;
import java.util.Objects;


/**
 * Created by IntelliJ IDEA
 * Author: Ahmed Al-Aishat
 * Date: 11/18/2020
 * Time: 4:53 PM
 */
@Entity
@Table(name = "users")
@Getter
@Setter
public class AppUser implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long userId;


    @Column(unique = true)
    private String username;

    private String password;

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public AppUser() {
    }

    public AppUser(String username, String password) {
        this.username = username;
        this.password = password;
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.userId, this.username, this.password);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (!(o instanceof AppUser))
            return false;
        AppUser user = (AppUser) o;
        return Objects.equals(this.userId, user.userId) && Objects.equals(this.username, user.username)
                && Objects.equals(this.password, user.password);
    }

    @Override
    public String toString() {
        return "User{" + "id=" + this.userId + ", username='" + this.username + '\'' + ", password='" + this.password + '\'' + '}';
    }
}
