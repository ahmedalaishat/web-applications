package com.alaishat.ahmed.webapplicationsproject.models.repositories;

import com.alaishat.ahmed.webapplicationsproject.models.AppUser;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface UserRepository extends PagingAndSortingRepository<AppUser, Long>,
        JpaSpecificationExecutor<AppUser> {
    AppUser findByUsername(String username);
}