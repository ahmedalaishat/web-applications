package com.alaishat.ahmed.webapplicationsproject.models;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by IntelliJ IDEA
 * Author: Ahmed Al-Aishat
 * Date: 11/21/2020
 * Time: 3:17 PM
 */
@Entity
@Table(name = "cars")
@Getter
@Setter
public class Car implements Serializable {
    public static final String ALL = "all";
    public static final String SOLD = "sold";
    public static final String UNSOLD = "unsold";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long carId;

    private String name;

    private Long price;

    private Integer numOfSeats;

    private Date saleDate;

    private Long salePrice;

    private String buyerName;

    @Version
   private Long version ;

    @CreationTimestamp
    private Date createdAt;

    @UpdateTimestamp
    private Date updatedAt;

    private String updatedBy;

    private String createdBy;

    public Car(String name, Long price, Integer numOfSeats) {
        this.name = name;
        this.price = price;
        this.numOfSeats = numOfSeats;
    }

    public Car() {
    }
}
