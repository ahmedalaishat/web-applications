package com.alaishat.ahmed.webapplicationsproject.models.repositories;

import com.alaishat.ahmed.webapplicationsproject.models.AppUser;
import com.alaishat.ahmed.webapplicationsproject.models.Car;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.Optional;

/**
 * Created by IntelliJ IDEA
 * Author: Ahmed Al-Aishat
 * Date: 11/21/2020
 * Time: 3:19 PM
 */
public interface CarRepository extends PagingAndSortingRepository<Car, Long>,
        JpaSpecificationExecutor<Car>, CrudRepository<Car, Long> {
    Optional<Car> findByCarId(Long id);

    Iterable<Car> findBySaleDateNotNull();

    Iterable<Car> findBySaleDateNull();
}
