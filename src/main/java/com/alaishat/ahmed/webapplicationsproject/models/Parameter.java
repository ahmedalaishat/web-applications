package com.alaishat.ahmed.webapplicationsproject.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

/**
 * Created by IntelliJ IDEA
 * Author: Ahmed Al-Aishat
 * Date: 11/25/2020
 * Time: 3:05 PM
 */
@Entity
@Data
@Table(name = "parameters")
@AllArgsConstructor
@NoArgsConstructor
public class Parameter {
    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Id
    @Column(name = "parameter_key")
    String key;

    String value;

    @CreationTimestamp
    private Date createdAt;

    @UpdateTimestamp
    private Date updatedAt;

    private String updatedBy;

    private String createdBy;

    public Parameter(String key, String value) {
        this.key = key;
        this.value = value;
    }
}
