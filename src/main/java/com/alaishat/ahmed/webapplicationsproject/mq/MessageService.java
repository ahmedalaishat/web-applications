package com.alaishat.ahmed.webapplicationsproject.mq;

import java.util.Map;

/**
 * Created by IntelliJ IDEA
 * Author: Ahmed Al-Aishat
 * Date: 11/21/2020
 * Time: 7:58 PM
 */
public interface MessageService {
    Map sendMessage(ReportMessage reportMessage);
}
