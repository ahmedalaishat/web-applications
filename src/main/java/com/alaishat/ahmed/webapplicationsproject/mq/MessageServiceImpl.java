package com.alaishat.ahmed.webapplicationsproject.mq;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * Created by IntelliJ IDEA
 * Author: Ahmed Al-Aishat
 * Date: 11/21/2020
 * Time: 7:59 PM
 */
@Service("messageService")
public class MessageServiceImpl implements MessageService {

    @Autowired
    MessageSender messageSender;

    @Override
    public Map sendMessage(ReportMessage reportMessage) {
        return messageSender.sendMessage(reportMessage);
    }
}