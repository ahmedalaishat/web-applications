package com.alaishat.ahmed.webapplicationsproject.mq;

import com.alaishat.ahmed.webapplicationsproject.util.ResponseUtil;
import com.google.gson.JsonObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * Created by IntelliJ IDEA
 * Author: Ahmed Al-Aishat
 * Date: 11/21/2020
 * Time: 6:47 PM
 */
@Component
public class MessageSender {

    @Autowired
    JmsTemplate jmsTemplate;

    public Map sendMessage(final ReportMessage reportMessage) {
        jmsTemplate.send(session -> session.createObjectMessage(reportMessage));
        return ResponseUtil.getResponseBody("Report request send successfully", new JsonObject());
    }
}
