package com.alaishat.ahmed.webapplicationsproject.mq;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by IntelliJ IDEA
 * Author: Ahmed Al-Aishat
 * Date: 11/21/2020
 * Time: 8:01 PM
 */
@Setter
@Getter
public class ReportMessage implements Serializable {
    private static final long serialVersionUID = 6529685098267757690L;

    public ReportMessage(String emailAddress, String content, Date date) {
        this.emailAddress = emailAddress;
        this.content = content;
        this.date = date;
    }

    public ReportMessage() {
    }

    private String emailAddress;

    private String content;

    private Date date;

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((emailAddress == null) ? 0 : emailAddress.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        ReportMessage other = (ReportMessage) obj;
        if (emailAddress == null) {
            if (other.emailAddress != null)
                return false;
        } else if (!emailAddress.equals(other.emailAddress))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "Message{" +
                "emailAddress='" + emailAddress + '\'' +
                ", content='" + content + '\'' +
                ", date=" + date +
                '}';
    }
}