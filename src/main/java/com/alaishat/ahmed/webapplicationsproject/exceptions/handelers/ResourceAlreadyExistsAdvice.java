package com.alaishat.ahmed.webapplicationsproject.exceptions.handelers;

import com.alaishat.ahmed.webapplicationsproject.exceptions.ResourceAlreadyExistsException;
import com.alaishat.ahmed.webapplicationsproject.util.ResponseUtil;
import com.google.gson.JsonObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.Map;

/**
 * Created by IntelliJ IDEA
 * Author: Ahmed Al-Aishat
 * Date: 11/21/2020
 * Time: 3:04 PM
 */
@RestControllerAdvice
public class ResourceAlreadyExistsAdvice {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @ResponseBody
    @ExceptionHandler(ResourceAlreadyExistsException.class)
    @ResponseStatus(HttpStatus.CONFLICT)
    Map resourceAlreadyExistsHandler(ResourceAlreadyExistsException e) {
        logger.error(e.getMessage());
        return ResponseUtil.getErrorBody(e.getMessage(), new JsonObject());
    }
}
