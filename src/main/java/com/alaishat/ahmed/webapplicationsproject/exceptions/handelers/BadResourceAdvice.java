package com.alaishat.ahmed.webapplicationsproject.exceptions.handelers;

import com.alaishat.ahmed.webapplicationsproject.exceptions.BadResourceException;
import com.alaishat.ahmed.webapplicationsproject.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.Map;

/**
 * Created by IntelliJ IDEA
 * Author: Ahmed Al-Aishat
 * Date: 11/21/2020
 * Time: 3:05 PM
 */
@RestControllerAdvice
public class BadResourceAdvice {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @ResponseBody
    @ExceptionHandler(BadResourceException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    Map badResourceHandler(BadResourceException e) {
        logger.error(e.getMessage());
        return ResponseUtil.getErrorBody(e.getMessage(), e.getErrorMessages());
    }
}