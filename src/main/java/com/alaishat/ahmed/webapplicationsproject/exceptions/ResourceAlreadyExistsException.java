package com.alaishat.ahmed.webapplicationsproject.exceptions;

/**
 * Created by IntelliJ IDEA
 * Author: Ahmed Al-Aishat
 * Date: 11/21/2020
 * Time: 3:12 PM
 */
public class ResourceAlreadyExistsException extends AppException {
    public ResourceAlreadyExistsException(String message) {
        super(message);
    }
}