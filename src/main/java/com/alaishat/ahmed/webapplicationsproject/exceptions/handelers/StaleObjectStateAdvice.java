package com.alaishat.ahmed.webapplicationsproject.exceptions.handelers;

import com.alaishat.ahmed.webapplicationsproject.exceptions.ResourceNotFoundException;
import com.alaishat.ahmed.webapplicationsproject.util.ResponseUtil;
import com.google.gson.JsonObject;
import org.hibernate.StaleObjectStateException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.Map;

/**
 * Created by IntelliJ IDEA
 * Author: Ahmed Al-Aishat
 * Date: 12/17/2020
 * Time: 5:12 PM
 */
@RestControllerAdvice
public class StaleObjectStateAdvice {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @ResponseBody
    @ExceptionHandler(StaleObjectStateException.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    Map resourceNotFoundHandler(StaleObjectStateException e) {
        logger.error(e.getMessage());
        return ResponseUtil.getErrorBody(e.getMessage(), new JsonObject());
    }
}
