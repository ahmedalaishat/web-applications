package com.alaishat.ahmed.webapplicationsproject.exceptions;

import com.google.gson.JsonArray;

/**
 * Created by IntelliJ IDEA
 * Author: Ahmed Al-Aishat
 * Date: 11/21/2020
 * Time: 3:12 PM
 */
public class BadResourceException extends AppException {
    private JsonArray errorMessages = new JsonArray();

    public BadResourceException(String message) {
        super(message);
    }

    public JsonArray getErrorMessages() {
        return errorMessages;
    }

    public void addErrorMessage(String message) {
        this.errorMessages.add(message);
    }
}